function init()
{
   timer = 20;
   step = 0;
   _global.k = 27;
   var _loc3_ = new Object();
   var me = this;
   _loc3_.onKeyDown = function()
   {
      if(Key.getCode() === 13)
      {
         me.send();
      }
   };
   Key.addListener(_loc3_);
   _global.rand = rand;
   main.input.field.onChanged = function()
   {
      me.timer = 200;
   };
   main.mainBut.stop();
}
function loop()
{
   timer--;
   switch(step)
   {
      case 0:
         if(timer < 0)
         {
            talk(getRandom(sent.hello));
            initWait();
         }
         break;
      case 1:
         if(timer < 0)
         {
            long++;
            _global.k = random(1000);
            if(long > 10)
            {
               talk(getRandom(sent.wait3));
            }
            else if(long > 3)
            {
               talk(getRandom(sent.wait2));
            }
            else
            {
               talk(getRandom(sent.wait));
            }
            timer = 150 + random(150) * (1 + long * 0.5);
            break;
         }
         break;
      case 2:
   }
   if(dial != null)
   {
      dial.timer--;
      if(dial.timer <= 0)
      {
         dial.removeMovieClip();
         dial = null;
         initWait();
      }
   }
}
function initWait()
{
   step = 1;
   long = 0;
   timer = 200 + random(200);
}
function send()
{
   var _loc5_ = main.input.field.text;
   var _loc4_ = formatString(_loc5_);
   if(_loc4_.length < 10)
   {
      _global.k = 333;
      talk(getRandom(sent.short));
      return undefined;
   }
   main.input.field.text = "";
   _global.k = 0;
   var _loc2_ = 0;
   while(_loc2_ < _loc4_.length)
   {
      var _loc3_ = ord(_loc4_.charAt(_loc2_));
      _global.k = _global.k + _loc3_ * (_loc2_ + 1);
      if(_loc3_ % _loc2_ == 0)
      {
         _global.k = _global.k ^ _loc3_;
      }
      _loc2_ = _loc2_ + 1;
   }
   lastPred = _loc5_;
   main.mainBut.gotoAndStop(2);
   lastAnswer = sent.answer[rand(sent.answer.length)];
   talk(lastAnswer);
}
function talk(txt)
{
   step = 2;
   main.cadre.socrate.talk(int(txt.length * 0.3));
   if(dial != null)
   {
      dial.removeMovieClip();
   }
   dial = main.attachMovie("bulle","dial",40);
   dial.field._width = Math.min(Math.max(70,txt.length * 6),170);
   dial.field.text = txt;
   dial.field._height = (dial.field.textHeight + 1) * 1.3;
   var _loc2_ = dial.field._width + 1;
   var _loc1_ = dial.field._height;
   dial.sq._width = _loc2_;
   dial.sq._height = _loc1_;
   dial.tr._x = _loc2_;
   dial.bl._y = _loc1_;
   dial.br._x = _loc2_;
   dial.br._y = _loc1_;
   dial.t._width = _loc2_;
   dial.b._width = _loc2_;
   dial.r._height = _loc1_;
   dial.l._height = _loc1_;
   dial.r._x = _loc2_;
   dial.b._y = _loc1_;
   dial._x = 110;
   dial._y = 132 - _loc1_ * 0.5;
   dial.timer = 40 + txt.length * 3;
}
function getRandom(a)
{
   return a[random(a.length)];
}
function rand(n)
{
   var _loc2_ = _global.k % n;
   _global.k = _global.k + (n + 1);
   return _loc2_;
}
function formatString(str)
{
   str = replace(str," ","");
   str = replace(str,"?","");
   str = replace(str,"-","");
   str = replace(str,"\'","");
   str = replace(str,"è","e");
   str = replace(str,"é","e");
   str = replace(str,"ê","e");
   return str;
}
function replace(str, search, replace)
{
   var _loc4_ = "";
   var _loc5_ = "";
   if(search.length == 1)
   {
      return str.split(search).join(replace);
   }
   var _loc2_ = str.indexOf(search);
   if(_loc2_ == -1)
   {
      return str;
   }
   do
   {
      _loc2_ = str.indexOf(search);
      _loc4_ = str.substring(0,_loc2_);
      str = str.substring(_loc2_ + search.length);
      _loc5_ = _loc5_ + (_loc4_ + replace);
   }
   while(str.indexOf(search) != -1);
   
   _loc5_ = _loc5_ + str;
   return _loc5_;
}
function launch()
{
   lv = new LoadVars();
   if(lastPred != null)
   {
      lv.lastPred = lastPred;
      lv.lastAnswer = lastAnswer;
   }
   lv.send(url,"","POST");
}
function launchPub(id)
{
   getURL(pubUrl[id],"POST");
}
sent = new Object();
sent.hello = ["Salut!","Bonjour!","Bonjour, tu te poses des questions ?","Bonjour, je peux t\'aider ?","Bonjour, tu veux que je reponde a tes questions ?","Bonjour, comment vas-tu ?"];
sent.wait = ["Tu as une question pour moi ?","Je peux répondre a n\'importe quelle question.","Si tu as une question importante, n\'hésites pas.","N\'aies pas peur, je ne me trompe jamais."];
sent.wait2 = ["Alors ca vient cette question ?","Vas-y, pose ta question, ne soit pas timide !","Tu n\'es pas tres bavard(e) ...","Bon ! On va pas y passer la nuit!","Tu ne veux pas me parler ?","Vas-y, poses une question... Je ne vais pas te mordre !","Je te fais peur ?"];
sent.wait3 = ["Tu connaitrai pas un bon site d\'élevage de lapin ?","Si tu n\'as pas de questions pour moi, je vais me jeter de cette falaise !","Depêches-toi, j\'ai mon cours de muscu dans 20 minutes!","Vite ! Y\'a Platon qui m\'attend, je dois l\'aider a repeindre son cheval.","C\'est une maison bleu...","Je mangerai bien des lentilles moi ce soir.","Je me demande ce que je fais là moi.."];
sent.answer = ["Oui.","Oui.","Oui.","Oui.","Oui.","Oui.","Oui.","Oui.","Oui.","Oui.","Oui.","Oui.","Oui","Oui, parfaitement, le contraire m\'etonnerait pas mal !","Oui, j\'en suis certains!","Oui, j\'en suis sur.","Oui, bien evidemment.","Affirmatif!","Surement.","Non.","Non.","Non.","Non.","Non.","Non.","Non.","Non.","Non.","Non.","Non.","Non.","Non.","Non.","Non.","Non.","Jamais!","Surement pas","Ca m\'etonnerait","Bien sur que non.","Je ne crois pas non..."];
sent.short = ["Pourrais tu me donner plus de détails ?","Peux tu etre plus précis ?","Il me faudrait de plus amples explications...","C\'est un peu court..."];
pubUrl = ["http://www.socratomancie.com/monlapin","http://www.socratomancie.com/frutiparc","http://www.socratomancie.com/kadokado","http://www.socratomancie.com/monzoo"];
init();
