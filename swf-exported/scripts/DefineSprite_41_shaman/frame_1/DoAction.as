function talk(n)
{
   t = 0;
   max = n;
   if(max == null)
   {
      max = 6;
   }
   gotoAndStop("talk0");
   play();
   setJoue();
   setExpression();
}
function endAnim()
{
   t--;
   max--;
   if(max == 0)
   {
      gotoAndStop(1);
      joue.gotoAndStop(1);
      joue2.gotoAndStop(1);
      crane.o0.gotoAndStop(1);
      crane.o1.gotoAndStop(1);
      crane.plis.gotoAndStop(1);
      return undefined;
   }
   gotoAndPlay("talk" + _global.rand(3));
   if(_global.rand(3) == 0)
   {
      setJoue();
   }
   if(_global.rand(4) == 0 && crane.o0.frame != 4)
   {
      crane.o0.gotoAndPlay("cligne");
      crane.o1.gotoAndPlay("cligne");
      return undefined;
   }
   if(t <= 0 && _global.rand(4) == 0)
   {
      setExpression();
   }
}
function setJoue()
{
   var _loc2_ = _global.rand(2) + 1;
   joue.gotoAndStop(_loc2_);
   joue2.gotoAndStop(_loc2_);
}
function setExpression()
{
   var _loc3_ = _global.rand(crane.plis._totalframes) + 1;
   var _loc2_ = _global.rand(4) + 1;
   crane.plis.gotoAndStop(_loc3_);
   crane.o0.gotoAndStop(_loc2_);
   crane.o1.gotoAndStop(_loc2_);
   crane.o0.frame = _loc2_;
   crane.o1.frame = _loc2_;
   t = 5;
}
stop();
